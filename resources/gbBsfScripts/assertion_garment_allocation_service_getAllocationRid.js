var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateAllocationRID:function(param_AllocationRID, response_AllocationRID)
  {
		//Verifying AllocationRID object now
		for (var key in param_AllocationRID) 
		{
  			if (param_AllocationRID.hasOwnProperty(key) && typeof param_AllocationRID[key] !== 'object')
  			{
  			    var responseValue = response_AllocationRID[key];
				var paramExpectedValue = param_AllocationRID[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_AllocationRID does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying AllocationRID Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
    	myAssertion.validateSkuList(param_AllocationRID, response_AllocationRID);
  },
  
  
  validateSkuList:function(param_AllocationRID, response_AllocationRID)
  {			
		var param_skus = param_AllocationRID['skus'];
		var response_skus = response_AllocationRID['skus'];
		
		if(typeof param_skus !=='undefined' && param_skus.length > 0)
		{	
			myAssertion.logDebug("Validating skus.");
			if(typeof response_skus ==='undefined' || response_skus== null || response_skus.length != param_skus.length)
			{
				myAssertion.assertError("Response does not have appropriate skus");
			}
			else
			{
				var alreadyMatched = new Object();
				
				//Check all skus by using hashing
				for (var index = 0; index < param_skus.length; index++)
				{
					var param_sku = param_skus[index];
					var match = false;
					
					if (typeof param_sku === 'undefined' || param_sku ==null)
					{
						continue;
					}
					
					for (var index_response = 0; index_response < response_skus.length; index_response++)
					{
						var response_sku = response_skus[index_response];
						
						if( alreadyMatched[index_response] == 1)
						{
							myAssertion.logDebug("Skipping. Response index already matched: " + index_response);
						}
						else if(String(param_sku) === String(response_sku))
						{
							match = true;
							alreadyMatched[index_response] = 1;
							break;
						}
					}
					
					if (match)
					{
						myAssertion.assertPass("Verifying skus. Expecting param_sku:" + param_sku + ", Received response_skus list: " + response_skus);
					}
					else
					{
						myAssertion.assertError("Response does not have valid skus. Expecting param_sku:" + param_sku + ", Received response_skus list: " + response_skus)
					}
				}
			}
		}
  	},
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify AllocationRid
		var param_AllocationRID = parametersJSON['allocation'];
		var response_AllocationRID = apiResponse['allocation'];
		if(typeof param_AllocationRID !=='undefined' && param_AllocationRID && typeof response_AllocationRID !=='undefined' && response_AllocationRID)
		{
			myAssertion.validateAllocationRID(param_AllocationRID, response_AllocationRID);
		}
		else
		{
			myAssertion.assertError("One of param or response AllocationRID JSON is not valid. param_AllocationRID: " + param_AllocationRID + ", response_AllocationRID: " + response_AllocationRID);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}