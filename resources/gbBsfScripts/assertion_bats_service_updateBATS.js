var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateBATS:function(param_BATS, response_BATS)
  {
		//Verifying BATS object now
		for (var key in param_BATS) 
		{
  			if (param_BATS.hasOwnProperty(key) && key != 'addBats' && key != 'setLastUpdateVar' && key != 'createDate' && key != 'lastUpdated' && key !='bats' && typeof param_BATS[key] !== 'object')
  			{
  			    var responseValue = response_BATS[key];
				var paramExpectedValue = param_BATS[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_BATS does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying BATS Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
		var bats = response_BATS['bats'];
		var createDate = response_BATS['createDate'];
		var updateDate = response_BATS['lastUpdated'];
		var addBats = param_BATS['addBats'];
    	var setLastUpdateVar = param_BATS['setLastUpdateVar'];
    			
		if(typeof bats === 'undefined' || bats == null || bats <= 0)
		{
			myAssertion.assertError("BATS.bats in JSON response is invalid. bats: " + bats + ", Expecting > 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying BATS bats. Valid non-zero bats received. Received: " + bats);
    	}
    	
    	if(typeof addBats === 'undefined' || addBats == null || addBats.length == 0)
		{
			myAssertion.logDebug("No addBats is provided. bats of BATS will not be cross verified.");
		}
		else if (addBats === bats)
		{
			myAssertion.assertPass("Verifying BATS bats. Received: " + bats + ", Expecting: " + addBats);
		}
		else
		{
			myAssertion.assertError("BATS.bats in JSON response is not same. Received: " + bats + ", Expecting: " + addBats);
		}
    		
    	if (!myAssertion.isValidISODate(createDate)) {
    		myAssertion.assertError("Response does not have valid createDate date. Received: " + createDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying createDate date. Valid ISO format received. Received: " + createDate);
    	}
    		
    	if (!myAssertion.isValidISODate(updateDate)) {
			myAssertion.assertError("Response does not have valid updateDate date. Received: " + updateDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying updateDate date. Valid ISO format received. Received: " + updateDate);
    	}
    	
    	if(typeof setLastUpdateVar === 'undefined' || setLastUpdateVar == null || setLastUpdateVar.length == 0)
		{
			myAssertion.logDebug("No setLastUpdateVar is provided. LastUpdate of BATS will not be set in any Jmeter variable.");
		}
		else
		{
			vars.put(setLastUpdateVar, updateDate);
			myAssertion.logDebug("setLastUpdateVar is provided. Extracted bats will set '" + setLastUpdateVar + "' as Jmeter variables");
		}
  },
  
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify BATS
		var param_BATS = parametersJSON['bats'];
		var response_BATS = apiResponse['bats'];
		if(typeof param_BATS !=='undefined' && param_BATS && typeof response_BATS !=='undefined' && response_BATS)
		{
			myAssertion.validateBATS(param_BATS, response_BATS);
		}
		else
		{
			myAssertion.assertError("One of param or response BATS JSON is not valid. param_BATS: " + param_BATS + ", response_BATS: " + response_BATS);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}