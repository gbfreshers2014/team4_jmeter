var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateRID:function(param_RID, response_RID)
  {
		//Verifying RID object now
		for (var key in param_RID) 
		{
  			if (param_RID.hasOwnProperty(key) && key != 'addRid' && key != 'setLastUpdateVar' && key != 'createDate' && key != 'lastUpdated' && key !='rid' && typeof param_RID[key] !== 'object' && key != 'stage')
  			{
  			    var responseValue = response_RID[key];
				var paramExpectedValue = param_RID[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_RID does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying RID Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
		var rid = response_RID['rid'];
		var createDate = response_RID['createDate'];
		var updateDate = response_RID['lastUpdated'];
		var addRid = param_RID['addRid'];
    	var setLastUpdateVar = param_RID['setLastUpdateVar'];
    	var type = response_RID['type'];

    	if(typeof type === 'undefined' || type == null || type.length == 0)
		{
			myAssertion.assertError("No type in response. Expected non-empty value.");
		}
		else
		{
			myAssertion.assertPass("Verifying RID type. Valid non-empty type received. Received: " + type);
		}
    			
		if(typeof rid === 'undefined' || rid == null || rid <= 0)
		{
			myAssertion.assertError("RID.rid in JSON response is invalid. rid: " + rid + ", Expecting > 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying RID rid. Valid non-zero rid received. Received: " + rid);
    	}
    	
    	if(typeof addRid === 'undefined' || addRid == null || addRid.length == 0)
		{
			myAssertion.logDebug("No addRid is provided. rid of RID will not be cross verified.");
		}
		else if (addRid === rid)
		{
			myAssertion.assertPass("Verifying RID rid. Received: " + rid + ", Expecting: " + addRid);
		}
		else
		{
			myAssertion.assertError("RID.rid in JSON response is not same. Received: " + rid + ", Expecting: " + addRid);
		}
    		
    	if (!myAssertion.isValidISODate(createDate)) {
    		myAssertion.assertError("Response does not have valid createDate date. Received: " + createDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying createDate date. Valid ISO format received. Received: " + createDate);
    	}
    		
    	if (!myAssertion.isValidISODate(updateDate)) {
			myAssertion.assertError("Response does not have valid updateDate date. Received: " + updateDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying updateDate date. Valid ISO format received. Received: " + updateDate);
    	}
    	
    	if(typeof setLastUpdateVar === 'undefined' || setLastUpdateVar == null || setLastUpdateVar.length == 0)
		{
			myAssertion.logDebug("No setLastUpdateVar is provided. LastUpdate of RID will not be set in any Jmeter variable.");
		}
		else
		{
			vars.put(setLastUpdateVar, updateDate);
			myAssertion.logDebug("setLastUpdateVar is provided. Extracted rid will set '" + setLastUpdateVar + "' as Jmeter variables");
		}
    		
    	myAssertion.validateRIDAttributes(param_RID, response_RID);
  },
  
  
  validateRIDAttributes:function(param_RID, response_RID)
  {			
		var param_ridAttributes = param_RID['ridAttributes'];
		var response_ridAttributes = response_RID['ridAttributes'];
		
		if(typeof param_ridAttributes !=='undefined' && param_ridAttributes.length > 0)
		{	
			myAssertion.logDebug("Validating RIDAttributes.");
			if(typeof response_ridAttributes ==='undefined' || response_ridAttributes== null || response_ridAttributes.length != param_ridAttributes.length)
			{
				myAssertion.assertError("Response does not have appropriate ridAttributes");
			}
			else
			{
				var alreadyMatched = new Object();
				
				//Check all ridAttributes by using hashing
				for (var index = 0; index < param_ridAttributes.length; index++)
				{
					var param_ridAttribute = param_ridAttributes[index];
					var param_attribute = param_ridAttribute["attribute"];
					var param_value = param_ridAttribute["value"];
					var match = false;
					
					if (typeof param_attribute === 'undefined' || param_attribute ==null || typeof param_value === 'undefined' || param_value ==null)
					{
						continue;
					}
					
					for (var index_response = 0; index_response < response_ridAttributes.length; index_response++)
					{
						var response_ridAttribute = response_ridAttributes[index_response];
						var response_attribute = response_ridAttribute["attribute"];
						var response_value = response_ridAttribute["value"];
						
						if( alreadyMatched[index_response] == 1)
						{
							myAssertion.logDebug("Skipping. Response index already matched: " + index_response);
						}
						else if((String(param_attribute) === String(response_attribute)) && (String(param_value) === String(response_value)))
						{
							match = true;
							alreadyMatched[index_response] = 1;
							break;
						}
					}
					
					if (match)
					{
						myAssertion.assertPass("Verifying ridAttribute. Expecting param_attribute:" + param_attribute + ",  param_value: " + param_value + ", Received response_attribute: " + response_attribute + ", response_value: " + response_value);
					}
					else
					{
						myAssertion.assertError("Response does not have valid ridAttribute. Expecting param_attribute:" + param_attribute + ",  param_value: " + param_value + ", Received response_attribute: " + response_attribute + ", response_value: " + response_value);
					}
				}
			}
		}
  	},
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify RID
		var param_RID = parametersJSON['rid'];
		var response_RID = apiResponse['rid'];
		if(typeof param_RID !=='undefined' && param_RID && typeof response_RID !=='undefined' && response_RID)
		{
			myAssertion.validateRID(param_RID, response_RID);
		}
		else
		{
			myAssertion.assertError("One of param or response RID JSON is not valid. param_RID: " + param_RID + ", response_RID: " + response_RID);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}