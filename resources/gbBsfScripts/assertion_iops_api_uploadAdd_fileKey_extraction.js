vars.put("fileKey", "");
var isAssertionFailed = false;
var failureMessage = "";

var argsLength = args.length;
if (argsLength > 0)
{
	OUT.println("Command line args received. fileKey will also set for jmeter variable '"+args[0]+"'");
	vars.put(args[0], "");
}

var responseCode = prev.getResponseCode();

if(!responseCode.equals("301"))
{
	failureMessage = "Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 301" ;
	isAssertionFailed = true;
}
else
{	
	try
	{
		vars.put("whileControllerCondition", "true");
		vars.put("whileControllerCounter", "1");
		var responseHeader =  prev.getResponseHeaders();	
	}	 
	catch (e)
	{
		failureMessage = "ResponseHeader is not a valid. FileKey cannot be extracted." ;
		isAssertionFailed = true;
	}
	
	if(typeof responseHeader !== 'undefined' && responseHeader)
	{
		
		var regexStr = "[\\?&]filekey=([^&#]*)";
		var regex = new RegExp( regexStr );
		var fileKeyArray = regex.exec( responseHeader );
		var fileKey = fileKeyArray[1];
		
		vars.put("fileKey", fileKey);
		if(argsLength > 0)
		{
			vars.put(args[0], fileKey);
		}
		
		if(typeof fileKey === 'undefined' || fileKey == null || fileKey === "")
		{
			failureMessage = "FileKey in response header is not valid. fileKey: " + fileKey ;
			isAssertionFailed = true;
		}
		else
		{
			OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [FileKey: " + fileKey + "]");
		}
	}
	else
	{
		failureMessage = "responseHeader is not a valid. responseHeader: " + responseHeader ;
		isAssertionFailed = true;
	}
}


if(isAssertionFailed)
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
}
