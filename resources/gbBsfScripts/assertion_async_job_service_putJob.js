vars.put("jobName", "");
var isAssertionFailed = false;
var failureMessage = "";

var argsLength = args.length;
if (argsLength > 0)
{
	vars.put(args[0], "");
}

var responseCode = prev.getResponseCode();

if(!responseCode.equals("200"))
{
	failureMessage = "Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200";
	isAssertionFailed = true;
}
else
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");

	try
	{
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		failureMessage = "Response is not a valid JSON Object";
		isAssertionFailed = true;
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse)
	{
		var jobName = apiResponse['jobName'];
		var code = apiResponse['code'];
		
		vars.put("jobName", jobName);
		if(argsLength > 0)
		{
			vars.put(args[0], jobName);
		}
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			failureMessage = "Code in JSON response is invalid. Code: " + code + ", Expecting: 0";
			isAssertionFailed = true;
		}
		
		if(typeof jobName === 'undefined' || jobName == null || jobName === "")
		{
			failureMessage = "JobName in JSON response is not valid. JobName: " + jobName;
			isAssertionFailed = true;
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}

if(isAssertionFailed)
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
}