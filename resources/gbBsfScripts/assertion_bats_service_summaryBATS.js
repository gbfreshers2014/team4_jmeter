var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateSummary:function(param_Summary, response_Summary)
  {
		var completedYesterday = response_Summary['completedYesterday'];
		var completedToday = response_Summary['completedToday'];
		var pendingCount = response_Summary['pendingCount'];
		
		if(typeof completedYesterday === 'undefined' || completedYesterday == null || completedYesterday < 0)
		{
			myAssertion.assertError("Summary.completedYesterday in JSON response is invalid. completedYesterday: " + completedYesterday + ", Expecting >= 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying Summary completedYesterday. Valid non-zero completedYesterday received. Received: " + completedYesterday);
    	}

    	if(typeof completedToday === 'undefined' || completedToday == null || completedToday < 0)
		{
			myAssertion.assertError("Summary.completedToday in JSON response is invalid. completedToday: " + completedToday + ", Expecting >= 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying Summary completedToday. Valid non-zero completedToday received. Received: " + completedToday);
    	}

		if(typeof pendingCount === 'undefined' || pendingCount == null || pendingCount < 0)
		{
			myAssertion.assertError("Summary.pendingCount in JSON response is invalid. pendingCount: " + pendingCount + ", Expecting >= 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying Summary pendingCount. Valid non-zero pendingCount received. Received: " + pendingCount);
    	}
  },
  
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify Summary
		var param_Summary = parametersJSON['summaryData'];
		var response_Summary = apiResponse['summaryData'];
		if(typeof param_Summary !=='undefined' && param_Summary && typeof response_Summary !=='undefined' && response_Summary)
		{
			myAssertion.validateSummary(param_Summary, response_Summary);
		}
		else
		{
			myAssertion.assertError("One of param or response Summary JSON is not valid. param_Summary: " + param_Summary + ", response_Summary: " + response_Summary);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}