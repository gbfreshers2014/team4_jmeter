var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateBATS:function(param_BATS, response_BATS)
  {
		//Verifying BATS object now
		for (var key in param_BATS) 
		{
  			if (param_BATS.hasOwnProperty(key) && key != 'addBats' && key != 'createDate' && key != 'lastUpdated' && key !='bats' && typeof param_BATS[key] !== 'object')
  			{
  			    var responseValue = response_BATS[key];
				var paramExpectedValue = param_BATS[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_BATS does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying BATS Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
		var bats = response_BATS['bats'];
		var createDate = response_BATS['createDate'];
		var updateDate = response_BATS['lastUpdated'];
		var addBats = param_BATS['addBats'];
		
		if(typeof bats === 'undefined' || bats == null || bats <= 0)
		{
			myAssertion.assertError("BATS.bats in JSON response is invalid. bats: " + bats + ", Expecting > 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying BATS bats. Valid non-zero bats received. Received: " + bats);
    	}
    	
    	if(typeof addBats === 'undefined' || addBats == null || addBats.length == 0)
		{
			myAssertion.logDebug("No addBats is provided. bats of BATS will not be cross verified.");
		}
		else if (addBats === bats)
		{
			myAssertion.assertPass("Verifying BATS bats. Received: " + bats + ", Expecting: " + addBats);
		}
		else
		{
			myAssertion.assertError("BATS.bats in JSON response is not same. Received: " + bats + ", Expecting: " + addBats);
		}
    		
    	if (!myAssertion.isValidISODate(createDate)) {
    		myAssertion.assertError("Response does not have valid createDate date. Received: " + createDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying createDate date. Valid ISO format received. Received: " + createDate);
    	}
    		
    	if (!myAssertion.isValidISODate(updateDate)) {
			myAssertion.assertError("Response does not have valid updateDate date. Received: " + updateDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying updateDate date. Valid ISO format received. Received: " + updateDate);
    	}
  },  
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		var param_BATSList = parametersJSON['bats'];
		var response_BATSList = apiResponse['bats'];
		
		if(typeof param_BATSList !=='undefined' && param_BATSList && typeof response_BATSList !=='undefined' && response_BATSList)
		{
		
			var batsExactMatch = parametersJSON['batsExactMatch'];
			if (typeof batsExactMatch === 'undefined' || batsExactMatch == null) {
				batsExactMatch  = true;
			}
			
			if (batsExactMatch)
			{
				//Validate length of response and request BATSs.
				if(param_BATSList.length != response_BATSList.length)
				{
					myAssertion.assertError("Response does not have all BATS list. Received: " + response_BATSList.length + ", Expecting: " + param_BATSList.length);
				}
				else
    			{
    				myAssertion.assertPass("Verifying length. Expecting: " + param_BATSList.length + ", Received: " + response_BATSList.length);
    			}
    		
    			//Checking All BATS
				for (var index = 0; index < param_BATSList.length; index++)
				{
					var param_BATS = param_BATSList[index];
					var response_BATS = response_BATSList[index];
				
					if(typeof param_BATS !=='undefined' && param_BATS && typeof response_BATS !=='undefined' && response_BATS)
					{
						myAssertion.assertPass("Verifying BATS entry for index: " + index);
						myAssertion.validateBATS(param_BATS, response_BATS);
					}
					else
					{
						myAssertion.assertError("Response does not have valid BATS param or response. Expected: " + param_BATS);
					}
				}
			}
			else
			{
				var maxAllowedBats = parametersJSON['maxAllowedBats'];
				if (typeof maxAllowedBats === 'undefined' || maxAllowedBats == null || maxAllowedBats < 0)
				{
		   			myAssertion.logDebug("MaxAllowedBats is not provided. Skipping length check.");
				}
				else if (maxAllowedBats >= response_BATSList.length)
				{
					myAssertion.assertPass("Verifying Response max length. maxAllowedBats: " + maxAllowedBats + ", Length: " + response_BATSList.length);
				}
				else
				{
					myAssertion.assertError("Length of response is more than maxAllowedBats. maxAllowedBats: " + maxAllowedBats + ", response_BATSList Length: " + response_BATSList.length);
				}
				
				//Checking All BATS
				for (var index = 0; index < response_BATSList.length; index++)
				{
					var response_BATS = response_BATSList[index];
				
					if(typeof response_BATS !=='undefined' && response_BATS)
					{
						myAssertion.assertPass("Verifying BATS entry for index: " + index);
						myAssertion.validateBATS(response_BATS, response_BATS);
					}
					else
					{
						myAssertion.assertError("Response does not have valid BATS entry. Expected: " + response_BATS + ", Received: " + response_BATS);
					}
				}
			}
		}
		else
		{
			myAssertion.assertError("One of param or response BATS list JSON is not valid. param_BATSList: " + param_BATSList + ", response_BATSList: " + response_BATSList);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}