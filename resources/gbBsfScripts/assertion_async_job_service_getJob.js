var putJobName = String(vars.get("jobName"));
var isAssertionFailed = false;
var failureMessage = "";

var responseCode = prev.getResponseCode();

if(!responseCode.equals("200"))
{
	failureMessage = "Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200" ;
	isAssertionFailed = true;
}
else
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");

	try
	{
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		failureMessage = "Response is not a valid JSON Object" ;
		isAssertionFailed = true;
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse)
	{
		var jobName = apiResponse['jobName'];
		var code = apiResponse['code'];
		var jobDefinition = apiResponse['jobDefinition'];
		var notificationMap = apiResponse['notificationMap'];	
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			failureMessage = "Code in JSON response is invalid. Code: " + code + ", Expecting: 0" ;
			isAssertionFailed = true;
		}

		if(typeof jobDefinition !== 'undefined' && jobDefinition)
		{
			var jobState = jobDefinition['jobState'];
			var jobName = jobDefinition['jobName'];
			var jobSubmitTime = jobDefinition['jobSubmitTime'];
			var paramMap = jobDefinition['paramMap'];
			
			if(typeof jobState === 'undefined' || !jobState || (jobState !== "complete" && jobState !== "pending" && jobState !== "running"))
			{
				failureMessage = "JobState in JSON response is not valid. JobState: " + jobState + ", Expected: {complete, pending, running}" ;
				isAssertionFailed = true;
			}
			
			if(typeof jobName === 'undefined' || jobName == null || jobName === "" || jobName !== putJobName)
			{
				failureMessage = "JobName in JSON response is not valid. JobName: " + jobName + ", Expected: " + putJobName ;
				isAssertionFailed = true;
			}
			
			//TODO: JobSubmit Time verification should come here
			//TODO: PARAM MAP validation should come here.
		}
		else
		{
			failureMessage = "JobDefinition in JSON response is not valid. JobDefinition: " + jobDefinition ;
			isAssertionFailed = true;
		}
			
		if(typeof notificationMap !== 'undefined' && notificationMap)
		{
			//TODO: Notification map validation should come here. Optional so even if it is null or undefined assertion should not be marked failed.
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse ;
		isAssertionFailed = true;
	}
}

if(isAssertionFailed)
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
}