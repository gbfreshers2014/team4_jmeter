var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateAllocationRun:function(param_AllocationRun, response_AllocationRun)
  {
		//Verifying AllocationRun object now
		for (var key in param_AllocationRun) 
		{
  			if (param_AllocationRun.hasOwnProperty(key) && key != 'postedDate' && typeof param_AllocationRun[key] !== 'object')
  			{
  			    var responseValue = response_AllocationRun[key];
				var paramExpectedValue = param_AllocationRun[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_AllocationRun does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying AllocationRun Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
		var postedDate = response_AllocationRun['postedDate'];
    		
    	if (!myAssertion.isValidISODate(postedDate)) {
    		myAssertion.assertError("Response does not have valid postedDate date. Received: " + postedDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying postedDate date. Valid ISO format received. Received: " + postedDate);
    	}
  },
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify AllocationRun
		var param_AllocationRun = parametersJSON['allocationRun'];
		var response_AllocationRun = apiResponse['allocationRun'];
		if(typeof param_AllocationRun !=='undefined' && param_AllocationRun && typeof response_AllocationRun !=='undefined' && response_AllocationRun)
		{
			myAssertion.validateAllocationRun(param_AllocationRun, response_AllocationRun);
		}
		else
		{
			myAssertion.assertError("One of param or response AllocationRun JSON is not valid. param_AllocationRun: " + param_AllocationRun + ", response_AllocationRun: " + response_AllocationRun);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}