var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
};

var paramsLength = Parameters.length;

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{				
		for (var key in parametersJSON) 
		{
  			if (parametersJSON.hasOwnProperty(key))
  			{
  			    var responseValue = apiResponse[key];
				var paramExpectedValue = parametersJSON[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("apiResponse does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
	}
	else
	{
		myAssertion.assertError("apiResponse is not a valid object. apiResponse: " + apiResponse);
	}
}