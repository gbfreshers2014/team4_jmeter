var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateTrackData:function(param_trackdata, response_trackdata)
  {	
		//Verifying shipment object now
		for (var key in param_trackdata) 
		{
  			if (param_trackdata.hasOwnProperty(key) && key != 'createDate' && key != 'updateDate' && key !='id' && typeof param_trackdata[key] !== 'object')
  			{
  			    var responseValue = response_trackdata[key];
				var paramExpectedValue = param_trackdata[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.logDebug("track data does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
					return false;
				}
				else
				{
					myAssertion.logDebug("Verifying track data Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
		
		var id = response_trackdata['id'];
		var createDate = response_trackdata['createDate'];
		var updateDate = response_trackdata['updateDate'];
    	
    	if(typeof id === 'undefined' || id == null || id <= 0)
		{
			myAssertion.logDebug("id in JSON response is invalid. id: " + id + ", Expecting > 0");
			return false;
		}
		else
   		{
   			myAssertion.logDebug("Verifying id. Valid non-zero id received. Received: " + id);
   		}
   		
   		if (!myAssertion.isValidISODate(createDate)) {
   			myAssertion.logDebug("Response does not have valid createDate date. Received: " + createDate + ", Expecting: Valid date in ISO format");
   			return false;
   		}
   		else
   		{
   			myAssertion.logDebug("Verifying createDate date. Valid ISO format received. Received: " + createDate);
   		}
   		
   		if (!myAssertion.isValidISODate(updateDate)) {
			myAssertion.logDebug("Response does not have valid updateDate date. Received: " + updateDate + ", Expecting: Valid date in ISO format");
			return false;
   		}
   		else
   		{
   			myAssertion.logDebug("Verifying updateDate date. Valid ISO format received. Received: " + updateDate);
   		}

   		return true;
  },

  validateTrackDataList:function(param_trackdatalist, response_trackdatalist)
  {			
		if(typeof param_trackdatalist !=='undefined' && param_trackdatalist.length > 0)
		{	
			if(typeof response_trackdatalist ==='undefined' || response_trackdatalist== null || response_trackdatalist.length != param_trackdatalist.length)
			{
				myAssertion.assertError("Response does not have appropriate trackDataList");
			}
			else
			{
				var alreadyMatched = new Object();
				
				//Check all shipmentItems by using hashing
				for (var index = 0; index < param_trackdatalist.length; index++)
				{
					var param_trackdata = param_trackdatalist[index];
					var match = false;
					
					if (typeof param_trackdata === 'undefined' || param_trackdata ==null)
					{
						continue;
					}
					
					for (var index_response = 0; index_response < response_trackdatalist.length; index_response++)
					{
						var response_trackdata = response_trackdatalist[index_response];
						
						if( alreadyMatched[index_response] == 1)
						{
							myAssertion.logDebug("Skipping. Response index already matched: " + index_response);
						}
						else if(myAssertion.validateTrackData(param_trackdata, response_trackdata))
						{
							match = true;
							alreadyMatched[index_response] = 1;
							break;
						}
					}
					
					if (match)
					{
						myAssertion.assertPass("Verifying track data match found. ");
					}
					else
					{
						myAssertion.assertError("Response does not have valid track data. " + param_trackdata["trackingCode"]);
					}
				}
			}
		}
  	},
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length > 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify Shipment
		var param_trackdatalist = parametersJSON['trackDataList'];
		var response_trackdatalist = apiResponse['trackDataList'];
		if(typeof param_trackdatalist !=='undefined' && param_trackdatalist && typeof response_trackdatalist !=='undefined' && response_trackdatalist)
		{	
			myAssertion.validateTrackDataList(param_trackdatalist, response_trackdatalist);
		}
		else
		{
			myAssertion.assertError("One of param or response track data JSON is not valid. param_trackdata: " + param_trackdatalist + ", response_trackdata: " + response_trackdatalist);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}