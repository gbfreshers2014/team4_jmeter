var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  }
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		var paramResponseCode = parametersJSON['responseCode'];
		var paramStatusCode = parametersJSON['statusCode'];
		var paramMessage = parametersJSON['message'];
		
		
		if(!responseCode.equals(paramResponseCode))
		{
			myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: " + paramResponseCode);
		}
		else
		{
			myAssertion.assertPass("Verifying status ResponseCode: " + responseCode + ", Expecting: " + paramResponseCode);
			AssertionResult.setFailure(false);
		}
		
		if(typeof code === 'undefined' || code == null || String(code) !== String(paramStatusCode))
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: " + paramStatusCode);
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: " + paramStatusCode);
		}
		
		if(typeof message === 'undefined' || message == null || message.length === 0)
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Non-Empty and Should have proper error message. If message contains 'exception' than it is invalid.");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Non-Empty and Should have proper error message. If message contains 'exception' than it is invalid.");
		}
		
		if(typeof paramMessage !== 'undefined' && paramMessage != null && paramMessage.length >= 0)
		{
			if (message.indexOf(paramMessage) != -1)
			{
				myAssertion.assertPass("Message in JSON response contains parameter message. Message: " + message + ", Expecting: contains " + paramMessage);
			}
			else
			{
				myAssertion.assertError("Error message in JSON response does not contains parameter message. Message: " + message + ", Expecting: contains " + paramMessage);
			}
		}
		else
		{
			myAssertion.assertPass("Skipping message content verification.");
		}
	}
	else
	{
		myAssertion.assertError("apiResponse is not a valid object. apiResponse: " + apiResponse);
	}
}