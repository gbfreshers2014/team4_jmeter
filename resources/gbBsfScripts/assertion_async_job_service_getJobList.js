var isAssertionFailed = false;
var failureMessage = "";

var argsLength = args.length;

for(var index = 0; index < argsLength; index++)
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "] GetJobList Checking for jobName [" +  String(vars.get(args[index])) + "]");
}

var responseCode = prev.getResponseCode();

if(!responseCode.equals("200"))
{
	failureMessage = "Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200" ;
	isAssertionFailed = true;
}
else
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");

	try
	{
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		failureMessage = "Response is not a valid JSON Object." ;
		isAssertionFailed = true;
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse)
	{
		var jobList = apiResponse['jobList'];
		var code = apiResponse['code'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			failureMessage = "Code in JSON response is invalid. Code: " + code + ", Expected: 0" ;
			isAssertionFailed = true;
		}

		if(typeof jobList !== 'undefined' && jobList)
		{
			if(argsLength > 0)
			{
				var foundJobNameCount = 0;
				for(var index=0; index < jobList.length; index++)
				{
					var jobDefinition = jobList[index];
					var jobName = jobDefinition['jobName'];
					
					for (var argsIndex = 0; argsIndex < argsLength ; argsIndex++)
					{
						var argJobName = String(vars.get(args[argsIndex]));
						if(argJobName === jobName)
						{
							var jobState = jobDefinition['jobState'];
							var jobSubmitTime = jobDefinition['jobSubmitTime'];
							var paramMap = jobDefinition['paramMap'];
							
							if(typeof jobState === 'undefined' || !jobState || (jobState !== "complete" && jobState !== "pending" && jobState !== "running"))
							{
								failureMessage = "JobState in JSON response is not valid. JobState: " + jobState + ", Expected: {complete, pending, running}" ;
								isAssertionFailed = true;
							}
				
							//TODO: JobSubmit Time verification should come here
							//TODO: PARAM MAP validation should come here.
							
							foundJobNameCount = foundJobNameCount + 1;
							break;
						}
					}
				}
				
				if(foundJobNameCount != argsLength)
				{
					failureMessage = "JobList does not have all the jobs. Jobs Found: " + foundJobNameCount + ", Expected: " + argsLength ;
					isAssertionFailed = true;
				}
			}
		}
		else
		{
			failureMessage = "JobList in JSON response is not valid array. JobList: " + jobList ;
			isAssertionFailed = true;
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse ;
		isAssertionFailed = true;
	}
}


if(isAssertionFailed)
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionisAssertionFailed [" + failureMessage + "]");
	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
}