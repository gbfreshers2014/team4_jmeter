var expectedCodeValue = 0;
var terminateWhileLoop = false;
var fileKey = vars.get("fileKey");

var argsLength = args.length;
if (argsLength > 0)
{
	expectedCodeValue = parseInt(args[0]);
}

OUT.println("[JMETER_INFO] [" + sampler.getName() + "], FileKey [" + fileKey + "], Expected CodeValue [" + expectedCodeValue + "]");

var responseCode = prev.getResponseCode();
if(!responseCode.equals("200"))
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "], FileKey [" + fileKey + "] Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
	terminateWhileLoop = true;
}
else
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "], FileKey [" + fileKey + "] [" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");

	try
	{
		eval('var apiResponse = ' + prev.getResponseDataAsString());
		var statusResponse =  apiResponse["status"];
		var codeValue = parseInt(statusResponse["code"]);
		
		var counter = parseInt(vars.get("whileControllerCounter"));
		var maxCounter = parseInt(props.getProperty("async_jmeter_thread_wait_iterations_functional"));

		if ( counter >= maxCounter || typeof codeValue === 'undefined' || typeof codeValue != 'number' || isNaN(codeValue) || null == codeValue || codeValue === expectedCodeValue )
		{ 
  		   	terminateWhileLoop = true;
		}

		OUT.println("[JMETER_INFO] [" + sampler.getName() + "], FileKey [" + fileKey + "], Counter [" + counter + "], MaxCounter [" + maxCounter + "], CodeValue [" + codeValue + "], ExpectedCodeValue [" + expectedCodeValue + "], TerminateWhileLoop [" + terminateWhileLoop + "]");
		counter = counter + 1;
		vars.put("whileControllerCounter", counter);
	} 
	catch (e)
	{
		OUT.println("[JMETER_ERROR] [" + sampler.getName() + "], FileKey [" + fileKey + "] Error parsing response. Terminating while loop.");
		terminateWhileLoop = true;
	}
}

if(terminateWhileLoop)
{
	vars.put("whileControllerCondition", "false");
}
